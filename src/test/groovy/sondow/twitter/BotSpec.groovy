package sondow.twitter

import com.sys1yagi.mastodon4j.api.entity.Status
import java.security.SecureRandom
import spock.lang.Specification

class BotSpec extends Specification {

    def "go should make posters post a post"() {
        setup:
        Bluesky bluesky = Mock()
        Tooter tooter = Mock()
        SecureRandom random = new SecureRandom([0x0, 0x1, 0x3] as byte[])
        Bot bot = new Bot(bluesky, tooter, random)
        Status mastoStatus = new Status();
        String message = '' +
                '╪═╪═╪═╪═╪═╪═╪═╪\n' +
                '　　　　　　🐏　　\n' +
                '　　🐖 🐏　　　　　\n' +
                '　　 🌵　　　　　　\n' +
                '　　　 🐏　🐄　　　\n' +
                '　　　　　　　　　\n' +
                '╪═╪═╪═╪═╪═╪═╪═╪'
        Zoo zoo = new Zoo('Animal emoji art', message)


        when:
        bot.go()

        then:
        1 * bluesky.post(message)
        1 * tooter.toot(zoo) >> mastoStatus
        0 * _._
    }
}
