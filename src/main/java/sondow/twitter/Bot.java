package sondow.twitter;

import java.security.SecureRandom;

/**
 * The main application logic.
 */
public class Bot {

    private Bluesky bluesky;

    /**
     * The object that does the tooting to Mastodon.
     */
    private Tooter tooter;

    private SecureRandom random;

    /**
     * Constructs a new bot with the specified Tweeter, for unit testing.
     */
    Bot(Bluesky bluesky, Tooter tooter, SecureRandom random) {
        this.bluesky = bluesky;
        this.tooter = tooter;
        this.random = random;
    }

    /**
     * Constructs a new bot.
     */
    public Bot() {
        this.random = new SecureRandom();
        BotConfig botConfig = new BotConfigFactory().configure();
        BlueskyConfig blueskyConfig = botConfig.getBlueskyConfig();
        if (blueskyConfig != null) {
            this.bluesky = new Bluesky(blueskyConfig);
        }
        MastodonConfig mastodonConfig = botConfig.getMastodonConfig();
        if (mastodonConfig != null) {
            this.tooter = new Tooter(mastodonConfig);
        }
    }

    /**
     * Performs the application logic.
     */
    public void go() {
        Zoo zoo = new ZooBuilder(random).build();
        String message = zoo.getBodyText();
        if (bluesky != null) {
            bluesky.post(message);
        }
        if (tooter != null) {
            tooter.toot(zoo);
        }
    }
}
