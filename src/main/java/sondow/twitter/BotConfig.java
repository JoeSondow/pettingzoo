package sondow.twitter;

/**
 * The place to store all the bot's configuration variables, mostly from environment variables read
 * in BotConfigFactory.
 */
public class BotConfig {

    private BlueskyConfig blueskyConfig;
    private MastodonConfig mastodonConfig;

    public BotConfig(BlueskyConfig blueskyConfig,
            MastodonConfig mastodonConfig) {
        this.blueskyConfig = blueskyConfig;
        this.mastodonConfig = mastodonConfig;
    }

    public BlueskyConfig getBlueskyConfig() {
        return blueskyConfig;
    }

    public MastodonConfig getMastodonConfig() {
        return mastodonConfig;
    }
}
